const express = require("express");

const {
 getUsers,getUser,createUser,updateUser,deleteUser
} = require("../controllers/users");

const User = require("../models/User");


const router = express.Router({ mergeParams: true });

const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

router.use(protect)
router.use(authorize('admin'))
//Anyhting below router.use will use the them(here it is using protect and authorize)m   


router
//   .route("/").get(authorize('admin'),getUsers)
.route("/")
    .get(advancedResults(User),getUsers)
    .post(createUser);
  
router
  .route("/:id")
    .get(getUser)
    .put(updateUser)
    .delete(deleteUser);
  
module.exports = router;
